package com.epam.rest.dto;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class SubscriptionResponseDto extends RepresentationModel<SubscriptionResponseDto> {
    private Long id;
    private Long userId;
    private String startDate;
}
