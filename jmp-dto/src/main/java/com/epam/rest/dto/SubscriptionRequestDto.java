package com.epam.rest.dto;

import lombok.Data;

@Data
public class SubscriptionRequestDto {
    private Long id;
    private Long userId;
}
