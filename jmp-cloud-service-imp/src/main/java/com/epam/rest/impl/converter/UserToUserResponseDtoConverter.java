package com.epam.rest.impl.converter;

import com.epam.rest.dto.User;
import com.epam.rest.dto.UserResponseDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component("toUserResponseDtoConverter")
public class UserToUserResponseDtoConverter implements Converter<User, UserResponseDto> {
    @Override
    public UserResponseDto convert(User source) {
        UserResponseDto userResponseDto = new UserResponseDto();
        userResponseDto.setId(source.getId());
        userResponseDto.setName(source.getName());
        userResponseDto.setSurname(source.getSurname());
        userResponseDto.setBirthday(source.getBirthday().toString());
        return userResponseDto;
    }
}
