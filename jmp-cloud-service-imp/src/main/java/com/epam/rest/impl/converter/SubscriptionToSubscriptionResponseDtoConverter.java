package com.epam.rest.impl.converter;

import com.epam.rest.dto.Subscription;
import com.epam.rest.dto.SubscriptionResponseDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component("toSubscriptionResponseDtoConverter")
public class SubscriptionToSubscriptionResponseDtoConverter implements Converter<Subscription, SubscriptionResponseDto> {
    @Override
    public SubscriptionResponseDto convert(Subscription source) {
        SubscriptionResponseDto subscriptionResponseDto = new SubscriptionResponseDto();
        subscriptionResponseDto.setId(source.getId());
        subscriptionResponseDto.setUserId(source.getUser().getId());
        subscriptionResponseDto.setStartDate(source.getStartDate().toString());
        return subscriptionResponseDto;
    }
}
