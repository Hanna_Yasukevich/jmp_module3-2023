package com.epam.rest.impl.controller;

import com.epam.rest.api.SubscriptionService;
import com.epam.rest.controller.ServiceController;
import com.epam.rest.dto.SubscriptionRequestDto;
import com.epam.rest.dto.SubscriptionResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ServiceControllerImpl implements ServiceController {

    @Autowired
    private SubscriptionService subscriptionService;

    public SubscriptionResponseDto createSubscription(SubscriptionRequestDto subscriptionRequestDto) {
        SubscriptionResponseDto dto = subscriptionService.createSubscription(subscriptionRequestDto);
        addLink(dto);
        return dto;

    }

    public SubscriptionResponseDto updateSubscription(SubscriptionRequestDto subscriptionRequestDto) {
        SubscriptionResponseDto dto = subscriptionService.updateSubscription(subscriptionRequestDto);
        addLink(dto);
        return dto;
    }

    public void deleteSubscription(Long id) {
        subscriptionService.deleteSubscription(id);
    }

    public SubscriptionResponseDto getSubscription(Long id) {
        SubscriptionResponseDto dto = subscriptionService.getSubscription(id);
        addLink(dto);
        return dto;
    }

    public List<SubscriptionResponseDto> getAllSubscription() {
        List<SubscriptionResponseDto> dtos = subscriptionService.getAllSubscription();
        dtos.forEach(this::addLink);
        return dtos;
    }

    private void addLink(SubscriptionResponseDto dto) {
        Link link = linkTo(methodOn(ServiceControllerImpl.class).getSubscription(dto.getId())).withSelfRel();
        dto.add(link);
    }
}
