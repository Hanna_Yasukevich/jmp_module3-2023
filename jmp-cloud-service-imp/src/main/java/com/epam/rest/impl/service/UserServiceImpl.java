package com.epam.rest.impl.service;

import com.epam.rest.api.UserService;
import com.epam.rest.dto.User;
import com.epam.rest.dto.UserRequestDto;
import com.epam.rest.dto.UserResponseDto;
import com.epam.rest.impl.exception.ResourceNotFoundException;
import com.epam.rest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    @Qualifier("toUserResponseDtoConverter")
    private Converter<User, UserResponseDto> toUserResponseDtoConverter;

    @Autowired
    @Qualifier("toUserConverter")
    private Converter<UserRequestDto, User> toUserConverter;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        User user = toUserConverter.convert(userRequestDto);
        user = userRepository.save(user);
        return toUserResponseDtoConverter.convert(user);
    }

    @Override
    @Transactional
    public UserResponseDto updateUser(UserRequestDto userRequestDto) {
        Optional<User> optionalUser = userRepository.findById(userRequestDto.getId());

        if (optionalUser.isPresent()){
            User user = toUserConverter.convert(userRequestDto);
            userRepository.save(user);
            return toUserResponseDtoConverter.convert(user);
        }

        throw new ResourceNotFoundException("User not found");
    }

    @Override
    public void deleteUser(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (RuntimeException e) {
            throw new ResourceNotFoundException("User not found");
        }
    }

    @Override
    public UserResponseDto getUser(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            return toUserResponseDtoConverter.convert(user);
        }

        throw new ResourceNotFoundException("User not found");
    }

    @Override
    public List<UserResponseDto> getAllUser() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users.stream()
                .map(user -> toUserResponseDtoConverter.convert(user))
                .collect(Collectors.toList());
    }
}
