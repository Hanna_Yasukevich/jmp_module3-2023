package com.epam.rest.impl.controller;

import com.epam.rest.api.UserService;
import com.epam.rest.controller.UserController;
import com.epam.rest.dto.UserRequestDto;
import com.epam.rest.dto.UserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class UserControllerImpl implements UserController {

    @Autowired
    private UserService userService;

    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        UserResponseDto dto = userService.createUser(userRequestDto);
        addLink(dto);
        return dto;
    }

    public UserResponseDto updateUser(UserRequestDto userRequestDto) {
        UserResponseDto dto = userService.updateUser(userRequestDto);
        addLink(dto);
        return dto;
    }

    public void deleteUser(Long id) {
        userService.deleteUser(id);
    }

    public UserResponseDto getUser(Long id) {
        UserResponseDto dto = userService.getUser(id);
        addLink(dto);
        return dto;
    }

    public ResponseEntity<List<UserResponseDto>> getAllUser() {
        try {
            List<UserResponseDto> dtos = userService.getAllUser();
            dtos.forEach(this::addLink);
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void addLink(UserResponseDto dto) {
        Link link = linkTo(methodOn(UserControllerImpl.class).getUser(dto.getId())).withSelfRel();
        dto.add(link);
    }
}
