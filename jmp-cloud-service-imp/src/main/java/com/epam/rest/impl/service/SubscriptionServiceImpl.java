package com.epam.rest.impl.service;

import com.epam.rest.api.SubscriptionService;
import com.epam.rest.dto.Subscription;
import com.epam.rest.dto.SubscriptionRequestDto;
import com.epam.rest.dto.SubscriptionResponseDto;
import com.epam.rest.impl.exception.ResourceNotFoundException;
import com.epam.rest.repository.SubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    @Autowired
    @Qualifier("toSubscriptionConverter")
    private Converter<SubscriptionRequestDto, Subscription> toSubscriptionConverter;

    @Autowired
    @Qualifier("toSubscriptionResponseDtoConverter")
    private Converter<Subscription, SubscriptionResponseDto> toSubscriptionResponseDtoConverter;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Override
    public SubscriptionResponseDto createSubscription(SubscriptionRequestDto subscriptionRequestDto) {
        Subscription subscription = toSubscriptionConverter.convert(subscriptionRequestDto);
        subscription = subscriptionRepository.save(subscription);
        return toSubscriptionResponseDtoConverter.convert(subscription);
    }

    @Override
    @Transactional
    public SubscriptionResponseDto updateSubscription(SubscriptionRequestDto subscriptionRequestDto) {
        Optional<Subscription> optionalSubscription = subscriptionRepository.findById(subscriptionRequestDto.getId());

        if (optionalSubscription.isPresent()){
            Subscription subscription = toSubscriptionConverter.convert(subscriptionRequestDto);
            subscriptionRepository.save(subscription);
            return toSubscriptionResponseDtoConverter.convert(subscription);
        }

        throw new ResourceNotFoundException("Subscription not found");
    }

    @Override
    public void deleteSubscription(Long id) {
        try {
            subscriptionRepository.deleteById(id);
        } catch (RuntimeException e) {
            throw new ResourceNotFoundException("Subscription not found");
        }
    }

    @Override
    public SubscriptionResponseDto getSubscription(Long id) {
        Optional<Subscription> optionalSubscription = subscriptionRepository.findById(id);

        if (optionalSubscription.isPresent()){
            Subscription subscription = optionalSubscription.get();
            return toSubscriptionResponseDtoConverter.convert(subscription);
        }

        throw new ResourceNotFoundException("Subscription not found");
    }

    @Override
    public List<SubscriptionResponseDto> getAllSubscription() {
        List<Subscription> subscriptions = new ArrayList<>();
        subscriptionRepository.findAll().forEach(subscriptions::add);
        return subscriptions.stream()
                .map(subscription -> toSubscriptionResponseDtoConverter.convert(subscription))
                .collect(Collectors.toList());
    }
}
