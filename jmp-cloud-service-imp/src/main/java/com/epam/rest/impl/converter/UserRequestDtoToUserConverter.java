package com.epam.rest.impl.converter;

import com.epam.rest.dto.User;
import com.epam.rest.dto.UserRequestDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component("toUserConverter")
public class UserRequestDtoToUserConverter implements Converter<UserRequestDto, User> {
    @Override
    public User convert(UserRequestDto source) {
        User user = new User();
        user.setId(source.getId());
        user.setName(source.getName());
        user.setSurname(source.getSurname());
        user.setBirthday(LocalDate.parse(source.getBirthday()));
        return user;
    }
}
