package com.epam.rest.impl.converter;

import com.epam.rest.dto.Subscription;
import com.epam.rest.dto.SubscriptionRequestDto;
import com.epam.rest.dto.User;
import com.epam.rest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("toSubscriptionConverter")
public class SubscriptionRequestDtoToSubscriptionConverter implements Converter<SubscriptionRequestDto, Subscription> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Subscription convert(SubscriptionRequestDto source) {
        Subscription subscription = new Subscription();
        subscription.setId(source.getId());
        subscription.setUser(defineUser(source.getUserId()));
        return subscription;
    }

    private User defineUser(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        return optionalUser.orElse(null);
    }
}
