package com.epam.rest.controller;

import com.epam.rest.dto.UserRequestDto;
import com.epam.rest.dto.UserResponseDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping(value = "/v1/users")
@Api(value="User", description="User API")
public interface UserController {

    @PostMapping
    @ApiOperation(value = "createUser", notes="create User",nickname = "createUser")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful creation",
                    response = UserResponseDto.class, responseContainer = "UserResponseDto") })
    UserResponseDto createUser(@RequestBody UserRequestDto userRequestDto);

    @PutMapping
    @ApiOperation(value = "updateUser", notes="update User",nickname = "updateUser")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful update",
                    response = UserResponseDto.class, responseContainer = "UserResponseDto") })
    UserResponseDto updateUser(@RequestBody UserRequestDto userRequestDto);

    @DeleteMapping("/{id}")
    void deleteUser(@PathVariable("id") Long id);

    @GetMapping("/{id}")
    @ApiOperation(value = "getUser", notes="get User",nickname = "getUser")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = UserResponseDto.class, responseContainer = "UserResponseDto") })
    UserResponseDto getUser(@PathVariable("id") Long id);

    @GetMapping
    @ApiOperation(value = "getUsers", notes="get Users",nickname = "getUsers")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = UserResponseDto.class, responseContainer = "List") })
    ResponseEntity<List<UserResponseDto>> getAllUser();
}
