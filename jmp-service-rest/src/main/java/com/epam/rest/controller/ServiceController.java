package com.epam.rest.controller;

import com.epam.rest.dto.SubscriptionRequestDto;
import com.epam.rest.dto.SubscriptionResponseDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@RequestMapping(value = "/v1/subscriptions")
@Api(value="Subscription", description="Subscription API")
public interface ServiceController {

    @PostMapping
    @ApiOperation(value = "createSubscription", notes="create Subscription",nickname = "createSubscription")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful creation",
                    response = SubscriptionResponseDto.class, responseContainer = "SubscriptionResponseDto") })
    SubscriptionResponseDto createSubscription(@RequestBody SubscriptionRequestDto subscriptionRequestDto);

    @PutMapping
    @ApiOperation(value = "updateSubscription", notes="update Subscription",nickname = "updateSubscription")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful update",
                    response = SubscriptionResponseDto.class, responseContainer = "SubscriptionResponseDto") })
    SubscriptionResponseDto updateSubscription(@RequestBody SubscriptionRequestDto subscriptionRequestDto);

    @DeleteMapping("/{id}")
    void deleteSubscription(@PathVariable("id") Long id);

    @GetMapping("/{id}")
    @ApiOperation(value = "getSubscription", notes="get Subscription",nickname = "getSubscription")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = SubscriptionResponseDto.class, responseContainer = "SubscriptionResponseDto") })
    SubscriptionResponseDto getSubscription(@PathVariable("id") Long id);

    @GetMapping
    @ApiOperation(value = "getSubscriptions", notes="get Subscriptions",nickname = "getSubscriptions")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Service not found"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = SubscriptionResponseDto.class, responseContainer = "List") })
    List<SubscriptionResponseDto> getAllSubscription();
}
