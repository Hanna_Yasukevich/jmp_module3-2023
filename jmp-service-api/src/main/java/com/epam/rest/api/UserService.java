package com.epam.rest.api;

import com.epam.rest.dto.UserRequestDto;
import com.epam.rest.dto.UserResponseDto;

import java.util.List;

public interface UserService {
    UserResponseDto createUser(UserRequestDto userRequestDto);
    UserResponseDto updateUser(UserRequestDto userRequestDto);
    void deleteUser(Long id);
    UserResponseDto getUser(Long id);
    List<UserResponseDto> getAllUser();
}
