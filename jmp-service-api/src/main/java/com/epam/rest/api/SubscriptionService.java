package com.epam.rest.api;

import com.epam.rest.dto.SubscriptionRequestDto;
import com.epam.rest.dto.SubscriptionResponseDto;

import java.util.List;

public interface SubscriptionService {
    SubscriptionResponseDto createSubscription(SubscriptionRequestDto subscriptionRequestDto);
    SubscriptionResponseDto updateSubscription(SubscriptionRequestDto subscriptionRequestDto);
    void deleteSubscription(Long id);
    SubscriptionResponseDto getSubscription(Long id);
    List<SubscriptionResponseDto> getAllSubscription();
}
